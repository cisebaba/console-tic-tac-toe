# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board
def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

current_board = [1,2,3,4,5,6,7,8,9]
current_player = "X"

for play in range(9):
    print_board(current_board)
    response = input(" where would you like to move " + current_player+ " ? ")
    move = int(response)
    current_board[move-1] = current_player

    # for i in current_board:
    #     i = int(i)
    #     # print("This is i:",i)
    #     # print("This is current board: " , current_board)
    #     print(current_board[i])
    #     if(current_board[i] == current_board[i+1] and current_board[i+1] == current_board[i+2]):
    #         print_board(current_board)
    #         print(current_board[i], "has won")
    #         exit()
    #     else:
    #         i = i + 3
    #         # print(i)

    if(current_board[0]==current_board[1]and current_board[1]==current_board[2]):
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    if(current_board[3]==current_board[4]and current_board[4]==current_board[5]):
        print_board(current_board)
        print(current_board[3], "has won")
        exit()
    if(current_board[6]==current_board[7]and current_board[7]==current_board[8]):
        print_board(current_board)
        print(current_board[6], "has won")
        exit()
    if(current_board[1]==current_board[4]and current_board[4]==current_board[7]):
        print_board(current_board)
        print(current_board[6], "has won")
        exit()
    if(current_board[2]==current_board[5]and current_board[5]==current_board[8]):
        print_board(current_board)
        print(current_board[6], "has won")
        exit()
    if(current_board[3]==current_board[6]and current_board[6]==current_board[9]):
        print_board(current_board)
        print(current_board[6], "has won")
        exit()

    if (current_player=="X"):
        current_player = "O"
    else:
        current_player = "X"


print("The game ended in a tie")
